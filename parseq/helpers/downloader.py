import requests 
import tqdm
import os
from log_parser.settings import MEDIA_ROOT


def download_file(url: str) -> str:
    """
    Download file with progress bar
    :param url: eg. http://www.almhuette-raith.at/apache-log/access.log
    :return: File path where file was saved
    """
    file_name = url.split('/')[-1]
    # check folder exist
    if not os.path.isdir(MEDIA_ROOT):
        os.makedirs(MEDIA_ROOT)
    file_path = os.path.join(MEDIA_ROOT, file_name)
    r = requests.get(url, stream=True)
    file_size = int(r.headers['Content-Length'])
    chunk_size = 1024
    num_bars = int(file_size / chunk_size)
    with open(file_path, 'wb') as fp:
        for chunk in tqdm.tqdm(r.iter_content(chunk_size=chunk_size),
                               total=num_bars,
                               unit='KB',
                               desc='Download file: {}'.format(file_name),
                               leave=True):
                fp.write(chunk)
    return file_path
