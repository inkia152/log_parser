import re
from datetime import datetime

import requests
import tqdm

from pygrok import Grok
from parseq.managers import BulkCreateManager
from parseq.models import Log


def stream_parse_file(url: str, num_lines=None):
    """
    Stream parse file with progress bar
    :param url: eg. http://www.almhuette-raith.at/apache-log/access.log
    :param num_lines: Number of lines in file for parsing
    :return:
    """
    file_name = url.split('/')[-1]
    r = requests.get(url, stream=True)
    file_size = int(r.headers['Content-Length'])
    num_bars = int(file_size / 1024)
    unit = 'KB'
    if num_lines:
        num_bars = num_lines
        unit = 'lines'
    grok = Grok('%{COMBINEDAPACHELOG} "-"')
    bulk_mgr = BulkCreateManager(chunk_size=1000)
    parse_progress_bar = tqdm.tqdm(desc="Parsing file: {}".format(file_name),
                                   unit=unit, total=num_bars)
    line_counter = 0
    for line in r.iter_lines(decode_unicode=True):
        if line_counter == num_lines:
            break
        parse_dict = grok.match(line)
        if parse_dict is None:
            continue
        temp_obj = Log(ip=re.match(r"(?:[0-9]{1,3}\.){3}[0-9]{1,3}", parse_dict.get('clientip')).group(),
                       date=datetime.strptime(parse_dict.get('timestamp'), '%d/%b/%Y:%H:%M:%S %z'),
                       method=parse_dict.get('verb') or 'UNKWN',
                       uri=parse_dict.get('request') or 'BAD URI',
                       status_code=parse_dict.get('response') or 0,
                       response_length=parse_dict.get('bytes') or 0
                       )
        bulk_mgr.add(temp_obj)
        parse_progress_bar.update(1)
        line_counter += 1
    bulk_mgr.done()
    parse_progress_bar.close()
