import django_filters
from parseq.models import Log


class LogFilter(django_filters.FilterSet):
    class Meta:
        model = Log
        fields = ['ip']
