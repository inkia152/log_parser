import copy
from django.db.models import Count, Sum, Q
from django.shortcuts import render

from parseq.filters import LogFilter
from parseq.models import Log
from parseq.tables import LogTable, TopLogTable


def log_list(request):
    table = LogTable(Log.objects.all())
    unique_ip = Log.objects.order_by().values_list('ip').distinct().count()
    query = Log.objects.values('ip').annotate(ip_count=Count('ip'),
                                              sum_byte=Sum('response_length'),
                                              get_count=Count('method', filter=Q(method='GET')),
                                              post_count=Count('method', filter=Q(method='POST')),
                                              put_count=Count('method', filter=Q(method='PUT')),
                                              delete_count=Count('method', filter=Q(method='DELETE'))
                                              ).order_by('-ip_count')
    # dirty patch for qs slice
    qs_copy = copy.copy(query)
    qs_copy = qs_copy[:10]

    top_10_table = TopLogTable(qs_copy)

    table_filter = LogFilter(request.GET, queryset=Log.objects.all())
    table.paginate(page=request.GET.get("page", 1), per_page=25)
    return render(request, "parseq/log_list.html", {
        "table": table,
        "filter": table_filter,
        "top_10": top_10_table,
        "unique_ip": unique_ip})

