import django_tables2 as tables
from parseq.models import Log


class LogTable(tables.Table):
    class Meta:
        model = Log
        template_name = "django_tables2/bootstrap.html"
        fields = ("ip", "date", "method", "uri", "status_code", "response_length")


class TopLogTable(tables.Table):
    ip = tables.Column()
    ip_count = tables.Column(verbose_name="Количество записей")
    sum_byte = tables.Column(verbose_name='Количество переданных байт')
    get_count = tables.Column(verbose_name='Количество GET запросов')
    post_count = tables.Column(verbose_name='Количество POST запросов')
    put_count = tables.Column(verbose_name='Количество PUT запросов')
    delete_count = tables.Column(verbose_name='Количество DELETE запросов')

    class Meta:
        template_name = "django_tables2/bootstrap.html"
