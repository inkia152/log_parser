from django.apps import AppConfig


class ParseqConfig(AppConfig):
    name = 'parseq'
