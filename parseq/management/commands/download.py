from django.core.management.base import BaseCommand
from parseq.helpers.parser import stream_parse_file


class Command(BaseCommand):
    help = 'Download and parse Apache log file'

    def handle(self, *args, **options):
        stream_parse_file(options.get('url'), num_lines=options.get('number_lines'))
        # file_path = download_file(options.get('url'))
        # parse_and_save_apache_log(file_path)

    def add_arguments(self, parser):
        parser.add_argument('number_lines', type=int, nargs='?', default=None)
        parser.add_argument('url', type=str)
