from django.db import models


class Log(models.Model):
    """
    Model for data from Apache access log
    """
    ip = models.GenericIPAddressField(null=False, verbose_name="IP Адрес")
    date = models.DateTimeField(verbose_name="Дата запроса")
    method = models.CharField(max_length=8, verbose_name="Метод запроса")
    uri = models.URLField(max_length=8200, verbose_name="URI Запроса")
    status_code = models.IntegerField(verbose_name="Код состояния")
    response_length = models.IntegerField(verbose_name="Длина ответа")

    def __str__(self):
        return "{}, {}".format(self.ip, self.method)
