FROM python:3.6.8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt requirements.txt
COPY ./log_parser /log_parser
COPY ./parseq /parseq

RUN pip install -r /requirements.txt
WORKDIR /log_parser

#ADD requirements.txt /log_parser/
#RUN pip install -r requirements.txt
#ADD . /log_parser/